package controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import util.DatabaseUtil;
import model.Animal;
import model.Programare;

/**
 * Controllers to link to sceenbuilder. 
 * @author avram
 *
 */
public class MainController implements Initializable{
	
	/**
	 * List of tags from SceneBuilder
	 */
	
	@FXML
	private ListView<String>listView;
	@FXML
	private TextArea name;
	@FXML
	private TextArea photo;
	@FXML
	private TextArea age;
	@FXML
	private TextArea race;
	@FXML
	private TextArea title;
	@FXML
	private TextArea spieces;
	@FXML
	private TextArea weight;
	@FXML
	private TextArea nameOwner;
	@FXML
	private TextArea mail;
	@FXML
	private TextArea phone;
	//@FXML
	
	//private ListView<String>listViewDate;
	
	/*public void populateMainListView() throws Exception {
		DatabaseUtil db=new DatabaseUtil();
		db.setUP();
		db.startTransaction();
		List<Animal> animalDBList=(List<Animal>)db.animalList();
		ObservableList<String>animalNamesList=getAnimalName(animalDBList);
		listView.setItems(animalNamesList);
		listView.refresh();
		db.closeEntityManager();
		
	}
	*/
	/**
	 * Populate the view data list from scenebuilder.
	 * @throws Exception
	 */
	public void populateMainListViewDate() throws Exception {
		DatabaseUtil db=new DatabaseUtil();
		db.setUP();
		db.startTransaction();
		List<Programare> programsDBList=(List<Programare>)db.programListFromMedic();
		ObservableList<String>programsList=getDate(programsDBList);
		listView.setItems(programsList);
		
		listView.getSelectionModel().selectedItemProperty().addListener((value,select1,select2)->{
			
			for(Programare f: programsDBList)
			{
				if(f.getData()==select2 )
				{	
			Animal a=f.getAnimal();
			Map<String,String>animals=new HashMap<>();
			animals.put("name",a.getNume());
			animals.put("age",String.valueOf(a.getVarsta()));
			animals.put("weight",String.valueOf(a.getGreutate()));
			animals.put("mail",a.getMail());
			animals.put("phone",a.getTelefon());
			animals.put("nameOwner",a.getProprietar());
			animals.put("id",String.valueOf(a.getAnimalID()));
			animals.put("race",a.getRasa());
			animals.put("spieces",a.getSpecie());
			name.setText(animals.get("name"));
			age.setText(animals.get("age"));
			weight.setText(animals.get("weight"));
			mail.setText(animals.get("mail"));
			phone.setText(animals.get("phone"));
			nameOwner.setText(animals.get("nameOwner"));
			title.setText("Fisa matricola:"+animals.get("id"));
			race.setText(animals.get("race"));
			spieces.setText(animals.get("spieces"));
			/*
			name.setAccessibleText(a.getNume());
			age.setAccessibleText(""+a.getVarsta());
			weight.setAccessibleText(""+a.getGreutate());
			mail.setAccessibleText(a.getMail());
			phone.setAccessibleText(a.getTelefon());
			nameOwner.setAccessibleText(a.getProprietar());
			title.setAccessibleText("Fisa matricola:"+a.getAnimalID());
			race.setAccessibleText(a.getRasa());
			spieces.setAccessibleText(a.getSpecie());
			*/
				}
			}
		});
		
		listView.refresh();
		db.closeEntityManager();
		
	}
	/**
	 * Get an observable list of animals name from DB table animal
	 */
	public ObservableList<String> getAnimalName(List<Animal>animals){
		ObservableList<String>names=FXCollections.observableArrayList();
		for(Animal a:animals) {
			names.add(a.getNume());
		}
		return names;
	}
	/**
	 * Get an observable list of dates from DB table programare
	 * @param programs
	 * @return
	 */
	public ObservableList<String> getDate(List<Programare>programs){
		ObservableList<String>dates=FXCollections.observableArrayList();
		for(Programare a:programs) {
			dates.add(a.getData());
		}
		return dates;
	}
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1){
		try {
			populateMainListViewDate();
			//populateMainListView();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
