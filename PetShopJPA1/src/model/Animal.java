package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int animalID;

	private double greutate;

	private String mail;

	private String nume;

	private String observatie;

	private String proprietar;

	private String rasa;

	private String specie;

	private String telefon;

	private int varsta;

	//bi-directional many-to-one association to Diagnostic
	@OneToMany(mappedBy="animal")
	private List<Diagnostic> diagnostics;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="animal")
	private List<Programare> programares;

	public Animal() {
	}

	public int getAnimalID() {
		return this.animalID;
	}

	public void setAnimalID(int animalID) {
		this.animalID = animalID;
	}

	public double getGreutate() {
		return this.greutate;
	}

	public void setGreutate(double greutate) {
		this.greutate = greutate;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getObservatie() {
		return this.observatie;
	}

	public void setObservatie(String observatie) {
		this.observatie = observatie;
	}

	public String getProprietar() {
		return this.proprietar;
	}

	public void setProprietar(String proprietar) {
		this.proprietar = proprietar;
	}

	public String getRasa() {
		return this.rasa;
	}

	public void setRasa(String rasa) {
		this.rasa = rasa;
	}

	public String getSpecie() {
		return this.specie;
	}

	public void setSpecie(String specie) {
		this.specie = specie;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public int getVarsta() {
		return this.varsta;
	}

	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}

	public List<Diagnostic> getDiagnostics() {
		return this.diagnostics;
	}

	public void setDiagnostics(List<Diagnostic> diagnostics) {
		this.diagnostics = diagnostics;
	}

	public Diagnostic addDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().add(diagnostic);
		diagnostic.setAnimal(this);

		return diagnostic;
	}

	public Diagnostic removeDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().remove(diagnostic);
		diagnostic.setAnimal(null);

		return diagnostic;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setAnimal(null);

		return programare;
	}

}