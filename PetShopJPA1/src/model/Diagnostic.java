package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the diagnostic database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnostic.findAll", query="SELECT d FROM Diagnostic d")
public class Diagnostic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int diagnosticID;

	private String data;

	private String descriere;

	private String titlu;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="animalID_1")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="personalMedicalID_1")
	private Personalmedical personalmedical;

	public Diagnostic() {
	}

	public int getDiagnosticID() {
		return this.diagnosticID;
	}

	public void setDiagnosticID(int diagnosticID) {
		this.diagnosticID = diagnosticID;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDescriere() {
		return this.descriere;
	}

	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}

	public String getTitlu() {
		return this.titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}