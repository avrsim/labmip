package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int programareID;

	private String data;

	private String ora;

	private String tipProgramare;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="animalID")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="personalMedicalID")
	private Personalmedical personalmedical;

	public Programare() {
	}

	public int getProgramareID() {
		return this.programareID;
	}

	public void setProgramareID(int programareID) {
		this.programareID = programareID;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOra() {
		return this.ora;
	}

	public void setOra(String ora) {
		this.ora = ora;
	}

	public String getTipProgramare() {
		return this.tipProgramare;
	}

	public void setTipProgramare(String tipProgramare) {
		this.tipProgramare = tipProgramare;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}