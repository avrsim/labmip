package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Main class contains test for CRUD functions.
 * @author avram
 *
 */
public class Main extends Application{

	
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			BorderPane root=(BorderPane)FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene= new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			Socket socket=new Socket("localhost",5000);
			BufferedReader echoes=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter stringToEcho= new PrintWriter(socket.getOutputStream(),true);
			String echoString;
			String echoPasword;
			String response;
			Scanner sc=new Scanner(System.in);
			do {
				System.out.println("User:");
				echoString=sc.nextLine();
				System.out.println("Pasword:");
				echoPasword=sc.nextLine();
				stringToEcho.println(echoString);
				stringToEcho.println(echoPasword);
				if(!echoString.equals("exit")) {
					response=echoes.readLine();
					System.out.println(response);
					if(response=="OK")
						launch(args);
				}
			}while(!echoString.equals("exit"));
			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		launch(args);
		
	}
	
}
