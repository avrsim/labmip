package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Personalmedical;
import model.Programare;

/**
 * A class of useful functions for connection to database and management of said connection.  
 * Contains CRUD for tables Animals, Personalmedical, Programare from database PetShop
 * @author avram
 *
 */

public class DatabaseUtil {
	
	public static EntityManagerFactory entityManagerFactory; 
	public static EntityManager entityManager;
	
	/**
	 * Sets up connection to database PetShop 
	 * @throws Exception
	 */
	public void setUP() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJPA1");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
/**
 * Create new Animal
 * @param animal
 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	/**
	 * Create new PersonalMedical
	 * @param personal
	 */
	public void savePersonalMedical(Personalmedical personal) {
		entityManager.persist(personal);
	}
	
	/**
	 * Create new Programare
	 * @param programare
	 */
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}
	
	/**
	 * Starts transactions with database
	 */
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	/**
	 * Commits made transaction
	 */
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	/**
	 * Closes connection with database
	 */
	public void closeEntityManager() {
		entityManager.close();
	}
	
	/**
	 * Read all animals.
	 */ 
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from PetShop.Animal",Animal.class).getResultList();
		for(Animal animal : results) {
			System.out.println("Animal: " + animal.getNume() + " has ID: "+ animal.getAnimalID()+ " is a: "+animal.getRasa()+" from spices:" +animal.getSpecie());
		}	
	}

	/**
	 * Read all medical personnel.
	 */
	public void printAllPersonalMedicalFromDB() {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from PetShop.Personalmedical",Personalmedical.class).getResultList();
		for(Personalmedical personal : results) {
			System.out.println("Personalul medical: " + personal.getNume() + " has ID: "+ personal.getPersonalMedicalID()+" is: "+personal.getSpecializare());
		}
			
	}
	
	/**
	 *Read all timetables. 
	 */
	public void printAllProgramareFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare",Programare.class).getResultList();
		for(Programare programare : results) {
			System.out.println("Programare: " +programare.getTipProgramare()+" has ID: "+ programare.getProgramareID() + " is on: "+ programare.getData() + " at: " + programare.getOra()+" for: "+programare.getAnimal().getNume()+" at personal: "+programare.getPersonalmedical().getNume());
		}
			
	}

	/**
	 * Update an animal from table Animal with animal
	 * @param animal
	 */
	public void alterAnimalFromDB(Animal animal) {

		Animal animalAux=(Animal)entityManager.find(Animal.class, animal.getAnimalID());
		animalAux.setNume(animal.getNume());
		animalAux.setRasa(animal.getRasa());
		animalAux.setSpecie(animal.getSpecie());
		//entityManager.createNativeQuery("Update PetShop.Animal set nume="+animal.getNume()+" where animalID="+animal.getAnimalID()).executeUpdate();
		//entityManager.createNativeQuery("Update PetShop.Animal set rasa="+animal.getRasa()+" where animalID="+animal.getAnimalID()).executeUpdate();
		//entityManager.createNativeQuery("Update PetShop.Animal set specie="+animal.getSpecie()+" where animalID="+animal.getAnimalID()).executeUpdate();
	}
	
	/**
	 * Update a medical personal from table Personalmedical with personal
	 * @param personal
	 */
	public void alterPersonalMedicalFromDB(Personalmedical personal) {
		Personalmedical personalAux=(Personalmedical)entityManager.find(Personalmedical.class,personal.getPersonalMedicalID());
		personalAux.setNume(personal.getNume());
		personalAux.setSpecializare(personal.getSpecializare());
		//entityManager.createNativeQuery("Update PetShop.Personalmedical set nume="+personal.getNume()+" where personalMedicalID="+personal.getPersonalMedicalID()).executeUpdate();
		//entityManager.createNativeQuery("Update PetShop.Personalmedical set specializare="+personal.getSpecializare()+" where personalMedicalID="+personal.getPersonalMedicalID()).executeUpdate();
	}
	
	/**
	 * Update a timetable from table Programare with programare
	 * @param programare
	 */
	public void alterProgramareFromDB(Programare programare) {
		Programare programareAux=(Programare)entityManager.find(Programare.class,programare.getProgramareID());
		programareAux.setData(programare.getData());
		programareAux.setOra(programare.getOra());
		programareAux.setTipProgramare(programare.getTipProgramare());
		programareAux.setAnimal(programare.getAnimal());
		programareAux.setPersonalmedical(programare.getPersonalmedical());
		//entityManager.createNativeQuery("Update PetShop.Programare set data="+programare.getData()+" where programareID="+programare.getProgramareID()).executeUpdate();
		//entityManager.createNativeQuery("Update PetShop.Programare set ora="+programare.getOra()+" where programareID="+programare.getProgramareID()).executeUpdate();
		//entityManager.createNativeQuery("Update PetShop.Programare set tipProgramare="+programare.getTipProgramare()+" where programareID="+programare.getProgramareID()).executeUpdate();
		//entityManager.createNativeQuery("Update PetShop.Programare set animalID="+programare.getAnimal().getAnimalID()+" where programareID="+programare.getProgramareID()).executeUpdate();
		//entityManager.createNativeQuery("Update PetShop.Programare set personalMedicalID="+programare.getPersonalmedical().getPersonalMedicalID()+" where programareID="+programare.getProgramareID()).executeUpdate();
	}
	

	/**
	 * Delete an animal with id indexID form table Animal
	 * @param indexID
	 */
	public void deleteAnimalFromDB(int indexID) {
		entityManager.createNativeQuery("Delete from PetShop.Animal where animalID="+indexID).executeUpdate();
	}
	
	/**
	 * Delete a medical personal with id indexID form table Personalmedical
	 * @param indexID
	 */
	public void deletePersonalMedicalFromDB(int indexID) {
		entityManager.createNativeQuery("Delete from PetShop.Personalmedical where personalMedicalID="+indexID).executeUpdate();
	}
	
	/**
	 * Delete a timetable with id indexID form table Programare
	 * @param indexID
	 */
	public void deleteProgramareFromDB(int indexID) {
		entityManager.createNativeQuery("Delete from PetShop.Programare where programareID="+indexID).executeUpdate();
	}
	
	/**
	 * Prints all timetables sorted by date showing the date, name of animal and name of medical personnel.
	 */
	public void printSortedAllProgramareFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare order by PetShop.Programare.data",Programare.class).getResultList();
		for(Programare programare : results) {
			System.out.println("Programare din data: " + programare.getData()+" for: "+programare.getAnimal().getNume()+" at personal: "+programare.getPersonalmedical().getNume());
	}
	}
	
	public List<Animal>animalList(){
		 List<Animal>animalList=(List<Animal>)entityManager.createQuery("Select a from Animal a",Animal.class).getResultList();
		return animalList;
	}
	public List<Programare>programListFromMedic(){
		 List<Programare>programList=(List<Programare>)entityManager.createQuery("Select a from Programare a",Programare.class).getResultList();
		return programList;
	}
}
