package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Echoer extends Thread{

	private Socket socket;
	
	public Echoer(Socket socket){
		this.socket=socket;
	}
	
	public void run() {
		 Map<String,String> utiliz=new HashMap<String,String>();
		try {
			BufferedReader input=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output=new PrintWriter(socket.getOutputStream(),true);
			
			while(true) {
				String echoString=input.readLine();
				System.out.println("Utilizator:"+echoString);
				if(echoString.equals("exit")) {
					break;
				}
				else
				{
					if(utiliz.containsKey(echoString)==true)
					{
						String echoPasword=input.readLine();
						if(utiliz.get(echoString)==echoPasword)
						{
							//conectare la PetShop
							output.println(echoString+" from server.");
						}
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

